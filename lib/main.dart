import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:location/location.dart';
import 'package:new_project/models/locationsModel.dart';
import 'package:new_project/models/currentPostion.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          body: FireMap()
      )
    );
  }
}

class FireMap extends StatefulWidget {
  @override
  State createState() => FireMapState();
}


class FireMapState extends State<FireMap> {
    GoogleMapController mapController;
    Location location = new Location();
    Firestore firestore = Firestore.instance;
    Geoflutterfire geo = Geoflutterfire();
    BehaviorSubject<double> radius = BehaviorSubject(seedValue: 100.0);
    Stream<dynamic> query;
    StreamSubscription subscription;
    List locations = [Locations(groupID: 1, userID: 2, latitude: 36.864381, longitude: 10.132390),
                      Locations(groupID: 1, userID: 3, latitude: 36.864402, longitude: 10.132541),
                      Locations(groupID: 1, userID: 4, latitude: 36.864413, longitude: 10.132501),
                      Locations(groupID: 1, userID: 5, latitude: 36.864397, longitude: 10.132587),

                      Locations(groupID: 1, userID: 2, latitude: 36.864429, longitude: 10.132874),
                      Locations(groupID: 1, userID: 3, latitude: 36.864431, longitude: 10.132904),
                      Locations(groupID: 1, userID: 4, latitude: 36.864438, longitude: 10.132963),
                      Locations(groupID: 1, userID: 5, latitude: 36.864439, longitude: 10.133023),

                      Locations(groupID: 1, userID: 2, latitude: 36.864444, longitude: 10.133262),
                      Locations(groupID: 1, userID: 3, latitude: 36.864440, longitude: 10.133283),
                      Locations(groupID: 1, userID: 4, latitude: 36.864442, longitude: 10.133315),
                      Locations(groupID: 1, userID: 5, latitude: 36.864441, longitude: 10.133292),

                      Locations(groupID: 1, userID: 2, latitude: 36.864427, longitude: 10.133370),
                      Locations(groupID: 1, userID: 3, latitude: 36.864430, longitude: 10.133400),
                      Locations(groupID: 1, userID: 4, latitude: 36.864420, longitude: 10.133370),
                      Locations(groupID: 1, userID: 5, latitude: 36.864412, longitude: 10.133359),

                      Locations(groupID: 1, userID: 2, latitude: 36.864444, longitude: 10.133262),
                      Locations(groupID: 1, userID: 3, latitude: 36.864440, longitude: 10.133283),
                      Locations(groupID: 1, userID: 4, latitude: 36.864442, longitude: 10.133315),
                      Locations(groupID: 1, userID: 5, latitude: 36.864441, longitude: 10.133292),

                      Locations(groupID: 1, userID: 2, latitude: 36.864429, longitude: 10.132874),
                      Locations(groupID: 1, userID: 3, latitude: 36.864431, longitude: 10.132904),
                      Locations(groupID: 1, userID: 4, latitude: 36.864438, longitude: 10.132963),
                      Locations(groupID: 1, userID: 5, latitude: 36.864439, longitude: 10.133023),

                      Locations(groupID: 1, userID: 2, latitude: 36.864381, longitude: 10.132390),
                      Locations(groupID: 1, userID: 3, latitude: 36.864402, longitude: 10.132541),
                      Locations(groupID: 1, userID: 4, latitude: 36.864413, longitude: 10.132501),
                      Locations(groupID: 1, userID: 5, latitude: 36.864397, longitude: 10.132587),

                      Locations(groupID: 1, userID: 2, latitude: 36.864429, longitude: 10.132874),
                      Locations(groupID: 1, userID: 3, latitude: 36.864431, longitude: 10.132904),
                      Locations(groupID: 1, userID: 4, latitude: 36.864438, longitude: 10.132963),
                      Locations(groupID: 1, userID: 5, latitude: 36.864439, longitude: 10.133023),

                      Locations(groupID: 1, userID: 2, latitude: 36.864444, longitude: 10.133262),
                      Locations(groupID: 1, userID: 3, latitude: 36.864440, longitude: 10.133283),
                      Locations(groupID: 1, userID: 4, latitude: 36.864442, longitude: 10.133315),
                      Locations(groupID: 1, userID: 5, latitude: 36.864441, longitude: 10.133292),

                      Locations(groupID: 1, userID: 2, latitude: 36.864427, longitude: 10.133370),
                      Locations(groupID: 1, userID: 3, latitude: 36.864430, longitude: 10.133400),
                      Locations(groupID: 1, userID: 4, latitude: 36.864420, longitude: 10.133370),
                      Locations(groupID: 1, userID: 5, latitude: 36.864412, longitude: 10.133359),

                      Locations(groupID: 1, userID: 2, latitude: 36.864444, longitude: 10.133262),
                      Locations(groupID: 1, userID: 3, latitude: 36.864440, longitude: 10.133283),
                      Locations(groupID: 1, userID: 4, latitude: 36.864442, longitude: 10.133315),
                      Locations(groupID: 1, userID: 5, latitude: 36.864441, longitude: 10.133292),];

  @override
  build(context) {
    location.onLocationChanged.listen((locationData) {
      if (locationData != null) {
        //_addGeoPoint(locationData);
      }
     });
    //  locations.forEach((userLocation) {
    //    return firestore.collection('locations').add({ 
    //       'longitude': userLocation.longitude,
    //       "latitude" : userLocation.latitude,
    //       'groupID': userLocation.groupID,
    //       'userID': userLocation.userID,
    //       'date' : DateTime.now()
    //     });
    //  });
    // widgets go here
      return Stack(
      children: [
        GoogleMap(
          
          initialCameraPosition: CameraPosition(target: LatLng(36.864381, 10.132390), zoom:17),
          onMapCreated: _onMapCreated,
          myLocationEnabled: true, // Add little blue dot for device location, requires permission from user
          mapType: MapType.satellite, 
          trackCameraPosition: true
        )]
                );
              }
               void _onMapCreated(GoogleMapController controller) {
                 final userLocations = Firestore.instance.collection('locations');
                 BitmapDescriptor icon = BitmapDescriptor.defaultMarker;
                 List<CurrentPosition> lastPositions = [CurrentPosition(latitude: 0,longitude: 0,userID: 2),
                 CurrentPosition(latitude: 0,longitude: 0,userID: 3),
                 CurrentPosition(latitude: 0,longitude: 0,userID: 4),
                 CurrentPosition(latitude: 0,longitude: 0,userID: 5),];
                 List<MarkerOptions> markers = [];
                 userLocations.getDocuments().then((snapshot) {
                    snapshot.documents.forEach((document) {                
                      CurrentPosition curPos = CurrentPosition(longitude: document.data["longitude"],latitude: document.data["latitude"],userID: document.data["userID"]);
                      if (document.data["userID"] == 2 ) {
                        icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure);
                      } else if (document.data["userID"] == 3) {
                        icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen);
                      } else if (document.data["userID"] == 4) {
                        icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange);
                      } else if (document.data["userID"] == 5) {
                        icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet);
                      }
                      var marker = MarkerOptions(
                            position: LatLng(curPos.latitude, curPos.longitude),
                            icon: icon
                        );
                      markers.add(marker);
                    });
                    var i = 0;
                      markers.forEach((marker) {
                        if (i <= 4) {
                          Timer(Duration(seconds: 3), () {
                            mapController.addMarker(marker);
                            i++;
                          });
                          
                        } else {
                          i = 0;
                          Timer(Duration(seconds: 2), () {
                            mapController.clearMarkers();
                          });
                        }
                        
                      });
                  });
                  
                setState(() {
                  mapController = controller;
                });
              }
            
            Future sleep1(){
                return new Future.delayed(const Duration(seconds: 10), () => "10");
            }
              _addMarker() {
                var marker = MarkerOptions(
                  position: mapController.cameraPosition.target,
                  icon: BitmapDescriptor.defaultMarker,
                  infoWindowText: InfoWindowText('Magic Marker', '🍄🍄🍄')
                
                );
                  mapController.addMarker(marker);

              }

              
      _animateToUser() async {
        var pos = await location.getLocation();

        mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
              target: LatLng(pos.latitude, pos.latitude),
              zoom: 17.0,
            )
          )
        );
      }
      Future<DocumentReference> _addGeoPoint(LocationData locationData) async {
        var pos = locationData;
        GeoFirePoint point = geo.point(latitude: pos.latitude, longitude: pos.longitude);
        return firestore.collection('locations').add({ 
          'position': point.data,
          'groupID': 1,
          'userID': 1,
          'date' : DateTime.now()
        });
}
void _updateMarkers(List<DocumentSnapshot> documentList) {
    print(documentList);
    mapController.clearMarkers();
    BitmapDescriptor icon = BitmapDescriptor.defaultMarker;
    documentList.forEach((DocumentSnapshot document) {
        GeoPoint pos = document.data['position']['geopoint'];
        double distance = document.data['distance'];
        if (document.data["userID"] == 2 ) {
           icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure);
        } else if (document.data["userID"] == 3) {
          icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen);
        } else if (document.data["userID"] == 4) {
          icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange);
        } else if (document.data["userID"] == 5) {
          icon = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet);
        }
        var marker = MarkerOptions(
          position: LatLng(document.data["latitude"], document.data["longitude"]),
          
          icon: icon,
          infoWindowText: InfoWindowText('Magic Marker', '$distance kilometers from query center')
        );


        mapController.addMarker(marker);
        documentList.remove(document);
    });
  }
   _startQuery() async {
    // Get users location
    var pos = await location.getLocation();
    double lat = pos.latitude;
    double lng = pos.longitude;


    // Make a referece to firestore
    var ref = firestore.collection('locations');
    GeoFirePoint center = geo.point(latitude: lat, longitude: lng);

    // subscribe to query
  }
   _updateQuery(value) {
     final zoomMap = {
       100.0: 12.0,
       200.0: 10.0,
       300.0: 7.0,
       400.0: 6.0,
       500.0: 5.0
     };
     final zoom = zoomMap[value];
     mapController.moveCamera(CameraUpdate.zoomTo(zoom));
      setState(() {
        radius.add(value);
      });
  }
    @override
  dispose() {
    subscription.cancel();
    super.dispose();
  }


}