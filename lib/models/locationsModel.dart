class Locations {
  int userID;
  int groupID;
  double longitude;
  double latitude;

  Locations({this.userID, this.groupID, this.longitude, this.latitude});

  Locations.fromJson(Map<String, dynamic> json) {
    userID = json['userID'];
    groupID = json['groupID'];
    longitude = json['longitude'];
    latitude = json['latitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userID'] = this.userID;
    data['groupID'] = this.groupID;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    return data;
  }
}