class CurrentPosition {
  int userID;
  double longitude;
  double latitude;

  CurrentPosition({this.userID, this.longitude, this.latitude});

  CurrentPosition.fromJson(Map<String, dynamic> json) {
    userID = json['userID'];
    longitude = json['longitude'];
    latitude = json['latitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userID'] = this.userID;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    return data;
  }
}